import discord
import face_recognition
import cv2
import numpy as np
import os
import glob
import asyncio
import aiohttp
from time import sleep
import sounddevice as sd
import sound

###Audio
async def connect(client,device_id, channel_id):
    stream = sound.PCMStream()
    channel = client.get_channel(channel_id)
    stream.change_device(device_id)

    voice = await channel.connect()
    voice.play(discord.PCMAudio(stream))

    print(f"Playing audio in {channel.name}")

DEFAULT = 0
sd.default.channels = 2
sd.default.dtype = "int16"
sd.default.latency = "low"
sd.default.samplerate = 48000

##Bot discord
client = discord.Client()
@client.event
async def on_ready():
    channel = client.get_channel(900408817983033355)
    await channel.send(file=discord.File('./capture.jpg'))
    for var in face_names:
        await channel.send(var.split('/')[-1].split('.')[0]+" est devant la porte")
    soundDevice=sound.query_devices().get('default')
    await connect(client,soundDevice,609480675954917400)
    sleep(10)
    await client.close()
 

    
faces_encodings = []
faces_names = []
cur_direc = os.getcwd()
path = os.path.join(cur_direc, './visage/')
list_of_files = [f for f in glob.glob(path+'*.jpg')]
number_files = len(list_of_files)
names = list_of_files.copy()

for i in range(number_files):
    globals()['image_{}'.format(i)] = face_recognition.load_image_file(list_of_files[i])
    globals()['image_encoding_{}'.format(i)] = face_recognition.face_encodings(globals()['image_{}'.format(i)])[0]
    faces_encodings.append(globals()['image_encoding_{}'.format(i)])
    names[i] = names[i].replace(cur_direc, "")  
    faces_names.append(names[i])

face_locations = []
face_encodings = []
face_names = []
process_this_frame = True
video_capture = cv2.VideoCapture(0)
while True:
    ret, frame = video_capture.read()
    small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)
    rgb_small_frame = small_frame[:, :, ::-1]
    if process_this_frame:
        face_locations = face_recognition.face_locations( rgb_small_frame)
        face_encodings = face_recognition.face_encodings( rgb_small_frame, face_locations)
        face_names = []
        if(len(face_encodings)>0):
            cv2.imwrite("capture.jpg", frame)
            for face_encoding in face_encodings:
                matches = face_recognition.compare_faces (faces_encodings, face_encoding)
                name = "Un inconnu"
                face_distances = face_recognition.face_distance( faces_encodings, face_encoding)
                best_match_index = np.argmin(face_distances)
                if matches[best_match_index]:
                    name = faces_names[best_match_index]
                face_names.append(name)
            client.start("ODkzMTQ3NDMwOTQ0NjQ5MjM2.YVXOXQ.SPZXj3O9sd_8CVHhcTfrPESWfYk")
    process_this_frame = not process_this_frame
    # Display the resulting image
    cv2.imshow('Video', frame)
    # Hit 'q' on the keyboard to quit!
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
